import turtle
import os,sys
import json
import pygame

from tkinter.messagebox import *

def read_json(path):
    files_brut = open(path, 'r')
    files = json.load(files_brut)
    return files

Bibli = read_json("test.json")
#text to print    
text_A = Bibli["Brique"]["-1"]["descri"]
text_B= Bibli["Brique"]["0"]["descri"]
text_c= Bibli["Brique"]["1"]["descri"]
text_d= Bibli["Brique"]["2"]["descri"]

widthscreen= 1680
heightscreen = 1050

size_carrer = 10
change =0
chozenOne = 0
step = 0

wn = turtle.Screen()
wn.title("Mix ta brique")


wn.tracer(0)
wn.setup(width = widthscreen, height = heightscreen)
#shape
pathbrique= ""
wn.addshape("logo.gif")
wn.addshape("welcome.gif")
wn.addshape("welcomeT.gif")
wn.addshape("rouge.gif")
wn.addshape("orange.gif")
wn.addshape("fonce.gif")
wn.addshape("jaune.gif")

wn.addshape("flamand.gif")
wn.addshape("debout.gif")

wn.addshape("Sculpture.gif")
wn.addshape("Ceramique.gif")
wn.addshape("Chainagesdepierres.gif")
wn.addshape("Jeudebriques.gif")

wn.bgpic("Presa.gif")

color="-1"
appar="flamand"
perso="brique"





#choix_A
choix_A = turtle.Turtle()
choix_A.speed(0)

choix_A.shape("logo.gif")
choix_A.color("black")
choix_A.shapesize(stretch_wid=1,stretch_len=1)
choix_A.penup()
choix_A.goto(0-(heightscreen/4*2)-100,0)

#choix_A_pen
choix_A_pen= turtle.Turtle()
choix_A_pen.speed(0)
choix_A_pen.color("black")
choix_A_pen.penup()
choix_A_pen.hideturtle()
choix_A_pen.goto(0-(heightscreen/4*2)-100, 0)
choix_A_pen.write(" ", align="center", font=("courrier", 24 , "normal"))


#choix_B
choix_B = turtle.Turtle()
choix_B.speed(0)
choix_B.shape("square")
choix_B.color("black")
choix_B.shapesize(stretch_wid=size_carrer,stretch_len=size_carrer)
choix_B.penup()
choix_B.goto(0-(heightscreen/4),0)

#choix_b_pen
choix_b_pen= turtle.Turtle()
choix_b_pen.speed(0)
choix_b_pen.color("black")
choix_b_pen.penup()
choix_b_pen.hideturtle()
choix_b_pen.goto(0-(heightscreen/4) , 0)
choix_b_pen.write(" ", align="center", font=("courrier", 24 , "normal"))
choix_b_pen.ht()


#choix_c
choix_c = turtle.Turtle()
choix_c.speed(0)
choix_c.shape("square")
choix_c.color("black")
choix_c.shapesize(stretch_wid=size_carrer,stretch_len=size_carrer)
choix_c.penup()
choix_c.goto((heightscreen/4),0)

#choix_c_pen
choix_c_pen= turtle.Turtle()
choix_c_pen.speed(0)
choix_c_pen.color("black")
choix_c_pen.penup()
choix_c_pen.hideturtle()
choix_c_pen.goto((heightscreen/4) , 0)
choix_c_pen.write(" ", align="center", font=("courrier", 24 , "normal"))
choix_c_pen.ht()


#choix_d
choix_d = turtle.Turtle()
choix_d.speed(0)
choix_d.shape("square")
choix_d.color("black")
choix_d.shapesize(stretch_wid=size_carrer,stretch_len=size_carrer)
choix_d.penup()
choix_d.goto((heightscreen/4*2)+100,0)

#choix_d_pen
choix_d_pen= turtle.Turtle()
choix_d_pen.speed(0)
choix_d_pen.color("black")
choix_d_pen.penup()
choix_d_pen.hideturtle()
choix_d_pen.goto((heightscreen/4*2)+100, 0)
choix_d_pen.write(" ", align="center", font=("courrier", 24 , "normal"))
choix_d_pen.ht()


#title
titre = turtle.Turtle()
titre.speed(0)
wn.colormode(255)
titre.color((196,50,25), (160, 200, 240))
titre.penup()
titre.hideturtle()
titre.goto(-350,350)
#titre.write("Mix ta brique", align="center", font=("courrier", 24 , "normal"))

#stept
stept = turtle.Turtle()
stept.speed(0)
stept.color("black")
stept.penup()
stept.hideturtle()
stept.goto(500,350)




#clean text
def choix_A_pen_text(text):
    choix_A_pen.clear()
    choix_A_pen.write(text, align="center", font=("courrier", 20 , "normal"))
def choix_b_pen_text(text):
    choix_b_pen.clear()
    choix_b_pen.write(text, align="center", font=("courrier", 20 , "normal"))
def choix_c_pen_text(text):
    choix_c_pen.clear()
    choix_c_pen.write(text, align="center", font=("courrier", 20 , "normal"))
def choix_d_pen_text(text):
    choix_d_pen.clear()
    choix_d_pen.write(text, align="center", font=("courrier", 20 , "normal"))

up=-1
down=2
#Function
def paddle_A_up():
    global chozenOne
    global change
    if(chozenOne!=up):
        chozenOne=chozenOne-1
        change=change-1
    

def paddle_A_down():
    global chozenOne
    global change
    if(chozenOne!=down):
        chozenOne=chozenOne+1
        change=change+1

cha =""
chb=""
chc=""
chd=""

def ok_space():
    global step
    global appar
    global brique
    global index 
    global perso
    global cha
    global chb
    global chc
    global chd
    global text_c
    global text_B
    global text_A
    global text_d 

    if step ==0:
        step +=1
        stept.clear()
        stept.write("Couleur de brique", align="center", font=("courrier", 20 , "normal"))
        choix_A.shape("rouge.gif")
        choix_B.shape("orange.gif")
        choix_c.shape("fonce.gif")
        choix_d.shape("jaune.gif")
    elif step ==1:
        brique=chozenOne
        step +=1
        stept.clear()
        stept.write("Appareillage de brique", align="center", font=("courrier", 20 , "normal"))
        titre.clear()
        titre.write(Bibli["Brique"]["appareillage"]["descri"], align="center", font=("courrier", 24 , "normal"))
        index = 2
        text_B= Bibli["Brique"]["appareillage"]["flamand"]
        text_c= Bibli["Brique"]["appareillage"]["debout"]
        choix_B.shape("flamand.gif")
        choix_c.shape("debout.gif")


        tout_monte()
    elif step ==2:
        if(chozenOne == 0):
            appar = "flamand"
        else:
            appar = "debout"
        
        index = Bibli["Brique"][color][appar]["index"]
        step +=1
        if index ==4:
            iteration = -1
        else:
            iteration = 0
        for i, f in Bibli["Brique"][color][appar].items():
            
            if iteration == 0:
                chb = i 
                text_B = f["descri"] 
                choix_B.shape(Bibli["Brique"]["perso"][i+"gif"])
                iteration +=1
            elif iteration == 1:
                chc = i 
                text_c = Bibli["Brique"]["perso"][i]
                choix_c.shape(Bibli["Brique"]["perso"][i+"gif"])
                iteration +=1
            elif iteration == 2:
                chd = i 
                text_d = Bibli["Brique"]["perso"][i]
                choix_d.shape(Bibli["Brique"]["perso"][i+"gif"])
                iteration +=1
            elif iteration ==-1:
                cha = i
                text_A = Bibli["Brique"]["perso"][i]
                choix_A.shape(Bibli["Brique"]["perso"][i+"gif"])
                iteration +=1
            
        stept.clear()
        stept.write("Ornementation de brique", align="center", font=("courrier", 20 , "normal"))
        tout_monte()
        titre.clear()
        titre.write(Bibli["Brique"]["perso"]["descri"], align="center", font=("courrier", 24 , "normal"))
    elif step ==3:
        if(chozenOne == 0):
            perso = chb
        elif chozenOne == 1:
            perso = chc
        elif chozenOne == 2:
            perso = chd
        elif chozenOne == -1:
            perso == cha
        step +=1
        stept.clear()
        stept.write(" ", align="center", font=("courrier", 20 , "normal"))
        tout_monte()
    elif step ==4:
        os.execl(sys.executable, sys.executable, *sys.argv)


    
def tutombe(choisie):
    y= choisie.ycor()
    if y>0-((heightscreen/5)*1):
        y -= 50
        choisie.sety(y)
        return False
    else:
        return True
def tumonte(choisie):
    y= choisie.ycor()
    if y<0:
        y += 50
        choisie.sety(y)
        return False
    else:
        return True

def show_suite():
    choix_A.st()
    choix_B.st()
    choix_c.st()
    choix_d.st()
    titre.st()

def hide_all():
    choix_A.ht()
    choix_B.ht()
    choix_c.ht()
    choix_d.ht()
    titre.ht()

def reset():
    choix_A_pen_text(" ")
    choix_b_pen_text(" ")
    choix_c_pen_text(" ")
    choix_d_pen_text(" ")


    
def positionnement(trois_quatre_deux):
    if trois_quatre_deux == 4:
        posiy(4)
    elif trois_quatre_deux ==3:
        posiy(3)
    elif trois_quatre_deux ==2:
        posiy(2)

def posiy(y):
    choix_A.setx(0-(heightscreen/y*(y/2))-100)
    choix_A_pen.setx(0-(heightscreen/y*(y/2))-100)
    choix_B.setx(0-(heightscreen/y))
    choix_b_pen.setx(0-(heightscreen/y))
    choix_c.setx((heightscreen/y))
    choix_c_pen.setx((heightscreen/y))
    choix_d.setx((heightscreen/y*(y/2))+100)
    choix_d_pen.setx((heightscreen/y*(y/2))+100)

def tout_monte():
    tumonte(choix_A)
    tumonte(choix_B)
    tumonte(choix_c)
    tumonte(choix_d)
    reset()
    global chozenOne
    chozenOne = 0


#keybord binding
wn.listen()
wn.onkeypress(paddle_A_up, "Left")
wn.onkeypress(paddle_A_down, "Right")
wn.onkeypress(ok_space,"space")
hide_all()
while step==0:
    wn.update()
titre.clear()
titre.ht()
index =4 
titre.write(Bibli["Brique"]["descri"], align="center", font=("courrier", 24 , "normal"))
wn.bgpic("nopic")
wn.bgcolor("white")
wn.setup(width = widthscreen, height = heightscreen)
show_suite()
titre.goto(-350,350)
#main loop

while step<4:
    wn.update()
    if(index==4):
        positionnement(4)
        show_suite()
        if(change>0):
            if(chozenOne==0):
                if(tumonte(choix_A)):
                    change=0
                    reset()
                tutombe(choix_B)
                choix_b_pen_text(text_B)
            elif(chozenOne==1):
                if(tumonte(choix_B)):
                    change=0
                    reset()
                tutombe(choix_c)
                choix_c_pen_text(text_c)
            elif(chozenOne==2):
                if(tumonte(choix_c)):
                    change=0
                    reset()
                tutombe(choix_d)
                choix_d_pen_text(text_d)
        elif(change<0):
            if(chozenOne==1):
                if(tumonte(choix_d)):
                    change=0
                    reset()
                tutombe(choix_c)
                choix_c_pen_text(text_c)
            elif(chozenOne==0):
                if(tumonte(choix_c)):
                    change=0
                    reset()
                tutombe(choix_B)
                choix_b_pen_text(text_B)
            elif(chozenOne==-1):
                if(tumonte(choix_B)):
                    change=0
                    reset()
                tutombe(choix_A)
                choix_A_pen_text(text_A)
    if index ==3:
        if(chozenOne==-1):
            chozenOne=0
        positionnement(3)
        show_suite()
        choix_A.ht()
        if(change>0):
            if(chozenOne==1):
                if(tumonte(choix_B)):
                    change=0
                    reset()
                tutombe(choix_c)
                choix_c_pen_text(text_c)
            elif(chozenOne==2):
                if(tumonte(choix_c)):
                    change=0
                    reset()
                tutombe(choix_d)
                choix_d_pen_text(text_d)
        elif(change<0):
            if(chozenOne==1):
                if(tumonte(choix_d)):
                    change=0
                    reset()
                tutombe(choix_c)
                choix_c_pen_text(text_c)
            elif(chozenOne==0):
                if(tumonte(choix_c)):
                    change=0
                    reset()
                tutombe(choix_B)
                choix_b_pen_text(text_B)
    if index ==2:
        show_suite()
        choix_A.ht()
        choix_B.st()
        choix_d.st()
        choix_d.ht()
        if(chozenOne==2):
            chozenOne =1
        if(chozenOne==-1):
            chozenOne=0
        positionnement(2)
        if(change>0):
            if(chozenOne==1):
                if(tumonte(choix_B)):
                    change=0
                    reset()
                tutombe(choix_c)
                choix_c_pen_text(text_c)
        elif(change<0):
            if(chozenOne==0):
                if(tumonte(choix_c)):
                    change=0
                    reset()
                tutombe(choix_B)
                choix_b_pen_text(text_B)
    
    #titre.clear()
    #titre.write(step, align="center", font=("courrier", 24 , "normal"))
wn.clearscreen()
wn.setup(width = widthscreen, height = heightscreen)
wn.bgpic("bye2.gif")
wn.bgcolor("white")

#facadedescri
facadedescri= turtle.Turtle()
facadedescri.speed(0)
facadedescri.color("black")
facadedescri.penup()
facadedescri.hideturtle()
facadedescri.goto((heightscreen/4*2), 0-((widthscreen/5)))
facadedescri.write(Bibli["Brique"][color][appar][perso]["descri"], align="center", font=("courrier", 20 , "normal"))

#composant
composant = turtle.Turtle()
composant.speed(0)
composant.color("black")
composant.penup()
composant.hideturtle()
composant.goto((heightscreen/4*2), ((widthscreen/5)*1))
composant.write(Bibli["Brique"][color][appar][perso]["name"] + "\n\n\n" + Bibli["Brique"][color]["name"] + "\n\n" + appar + "\n\n" + perso , align="left",font=("courrier", 20 , "normal"))
wn.onkeypress(ok_space,"space")
while step==4:
    wn.update()

    


#partie ou je traite juste les data lue
            
    
    