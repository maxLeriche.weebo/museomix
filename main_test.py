import turtle
import os,sys
import json
import pygame

def read_json(path):
    files_brut = open(path, 'r')
    files = json.load(files_brut)
    return files

#text to print    
text_A = "Je suis un text \nafficher dans la zone A"
text_B= "Je suis un text \nafficher dans la zone B"
text_c= "Je suis un text \nafficher dans la zone C"
text_d= "je suis un text \nafficher dans la zone D" 

widthscreen= 1680
heightscreen = 1050

size_carrer = 10
change =0
chozenOne = 0
step = 0

wn = turtle.Screen()
wn.title("Mix ta brique")


wn.tracer(0)
wn.setup(width = widthscreen, height = heightscreen)
#shape
pathbrique= ""
wn.addshape("logo.gif")
wn.addshape("welcome.gif")
wn.bgpic("welcome.gif")

color="nope"
appar="nope"
perso="nope"





#choix_A
choix_A = turtle.Turtle()
choix_A.speed(0)

choix_A.shape("logo.gif")
choix_A.color("black")
choix_A.shapesize(stretch_wid=1,stretch_len=1)
choix_A.penup()
choix_A.goto(0-(heightscreen/4*2)-100,0)

#choix_A_pen
choix_A_pen= turtle.Turtle()
choix_A_pen.speed(0)
choix_A_pen.color("black")
choix_A_pen.penup()
choix_A_pen.hideturtle()
choix_A_pen.goto(0-(heightscreen/4*2)-100, 0)
choix_A_pen.write(" ", align="center", font=("courrier", 24 , "normal"))


#choix_B
choix_B = turtle.Turtle()
choix_B.speed(0)
choix_B.shape("square")
choix_B.color("black")
choix_B.shapesize(stretch_wid=size_carrer,stretch_len=size_carrer)
choix_B.penup()
choix_B.goto(0-(heightscreen/4),0)

#choix_b_pen
choix_b_pen= turtle.Turtle()
choix_b_pen.speed(0)
choix_b_pen.color("black")
choix_b_pen.penup()
choix_b_pen.hideturtle()
choix_b_pen.goto(0-(heightscreen/4) , 0)
choix_b_pen.write(" ", align="center", font=("courrier", 24 , "normal"))
choix_b_pen.ht()


#choix_c
choix_c = turtle.Turtle()
choix_c.speed(0)
choix_c.shape("square")
choix_c.color("black")
choix_c.shapesize(stretch_wid=size_carrer,stretch_len=size_carrer)
choix_c.penup()
choix_c.goto((heightscreen/4),0)

#choix_c_pen
choix_c_pen= turtle.Turtle()
choix_c_pen.speed(0)
choix_c_pen.color("black")
choix_c_pen.penup()
choix_c_pen.hideturtle()
choix_c_pen.goto((heightscreen/4) , 0)
choix_c_pen.write(" ", align="center", font=("courrier", 24 , "normal"))
choix_c_pen.ht()


#choix_d
choix_d = turtle.Turtle()
choix_d.speed(0)
choix_d.shape("square")
choix_d.color("black")
choix_d.shapesize(stretch_wid=size_carrer,stretch_len=size_carrer)
choix_d.penup()
choix_d.goto((heightscreen/4*2)+100,0)

#choix_d_pen
choix_d_pen= turtle.Turtle()
choix_d_pen.speed(0)
choix_d_pen.color("black")
choix_d_pen.penup()
choix_d_pen.hideturtle()
choix_d_pen.goto((heightscreen/4*2)+100, 0)
choix_d_pen.write(" ", align="center", font=("courrier", 24 , "normal"))
choix_d_pen.ht()


#title
titre = turtle.Turtle()
titre.speed(0)
wn.colormode(255)
titre.color((196,50,25), (160, 200, 240))
titre.penup()
titre.hideturtle()
titre.goto(0,350)
#titre.write("Mix ta brique", align="center", font=("courrier", 24 , "normal"))

#clean text
def choix_A_pen_text(text):
    choix_A_pen.clear()
    choix_A_pen.write(text, align="center", font=("courrier", 20 , "normal"))
def choix_b_pen_text(text):
    choix_b_pen.clear()
    choix_b_pen.write(text, align="center", font=("courrier", 20 , "normal"))
def choix_c_pen_text(text):
    choix_c_pen.clear()
    choix_c_pen.write(text, align="center", font=("courrier", 20 , "normal"))
def choix_d_pen_text(text):
    choix_d_pen.clear()
    choix_d_pen.write(text, align="center", font=("courrier", 20 , "normal"))

up=-1
down=2
#Function
def paddle_A_up():
    global chozenOne
    global change
    if(chozenOne!=up):
        chozenOne=chozenOne-1
        change=change-1
    

def paddle_A_down():
    global chozenOne
    global change
    if(chozenOne!=down):
        chozenOne=chozenOne+1
        change=change+1

def ok_space():
    global step
    if step ==0:
        step +=1
    elif step ==1:
        brique=chozenOne
        step +=1
    elif step ==2:
        appar=chozenOne
        step +=1
    elif step ==3:
        perso=chozenOne
        step +=1
    elif step ==4:
        os.execl(sys.executable, sys.executable, *sys.argv)


    
def tutombe(choisie):
    y= choisie.ycor()
    if y>0-((heightscreen/5)*1):
        y -= 50
        choisie.sety(y)
        return False
    else:
        return True
def tumonte(choisie):
    y= choisie.ycor()
    if y<0:
        y += 50
        choisie.sety(y)
        return False
    else:
        return True

def show_suite():
    choix_A.st()
    choix_B.st()
    choix_c.st()
    choix_d.st()
    titre.st()

def hide_all():
    choix_A.ht()
    choix_B.ht()
    choix_c.ht()
    choix_d.ht()
    titre.ht()

def reset():
    choix_A_pen_text(" ")
    choix_b_pen_text(" ")
    choix_c_pen_text(" ")
    choix_d_pen_text(" ")
    


#keybord binding
wn.listen()
wn.onkeypress(paddle_A_up, "Left")
wn.onkeypress(paddle_A_down, "Right")
wn.onkeypress(ok_space,"space")

hide_all()
while step==0:
    wn.update()
    
index = 4
titre.write("Mix ta brique", align="center", font=("courrier", 24 , "normal"))
wn.bgpic("nopic")
wn.bgcolor("white")
wn.setup(width = widthscreen, height = heightscreen)
show_suite()

#main loop

while step<4:
    wn.update()
    if(index==4):
        if(change>0):
            if(chozenOne==0):
                if(tumonte(choix_A)):
                    change=0
                    reset()
                tutombe(choix_B)
                choix_b_pen_text(text_B)
            elif(chozenOne==1):
                if(tumonte(choix_B)):
                    change=0
                    reset()
                tutombe(choix_c)
                choix_c_pen_text(text_c)
            elif(chozenOne==2):
                if(tumonte(choix_c)):
                    change=0
                    reset()
                tutombe(choix_d)
                choix_d_pen_text(text_d)
        elif(change<0):
            if(chozenOne==1):
                if(tumonte(choix_d)):
                    change=0
                    reset()
                tutombe(choix_c)
                choix_c_pen_text(text_c)
            elif(chozenOne==0):
                if(tumonte(choix_c)):
                    change=0
                    reset()
                tutombe(choix_B)
                choix_b_pen_text(text_B)
            elif(chozenOne==-1):
                if(tumonte(choix_B)):
                    change=0
                    reset()
                tutombe(choix_A)
                choix_A_pen_text(text_A)
    
    titre.clear()
    titre.write(step, align="center", font=("courrier", 24 , "normal"))

#wn.clearscreen()
#partie ou je traite juste les data lue
            
    
    